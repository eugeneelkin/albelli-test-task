import * as React from "react";
import * as ReactDOM from "react-dom";
import {IComponentDescriptor} from "./descriptors/propertyDescriptors";

export interface IButtonDescriptor extends IComponentDescriptor {
    onClickCallback: (e: React.MouseEvent<HTMLButtonElement>) => void;
    disabled: boolean;
    className: string;
}

export default class Button extends React.Component<IButtonDescriptor> {
    public render() {
        return (
            <button
                className={this.props.className + (this.props.disabled ? "-disabled" : "") }
                type="button"
                onClick={ this.props.disabled ? (e) => { e.preventDefault(); } : (e) => this.props.onClickCallback(e) }
                disabled={this.props.disabled}
            >{this.props.content}</button>
        );
    }
}
