import * as React from "react";
import * as ReactDOM from "react-dom";
import * as PropertyDescriptors from "./descriptors/propertyDescriptors";

export enum ValidationFieldType {
    text = 1,
    textarea,
}

export interface IValidationFieldDescriptor {
    type: ValidationFieldType;
    isRequired?: boolean;
    regExp?: RegExp;
    errMsg?: string;
    value: string;
    fieldKey: string;
    changeHandler: (e: any, fieldKey: string) => Promise<string>;
    updateFieldValidationStatus: (isValid: boolean, fieldKey: string) => void;
}

interface IValidationFieldState {
    isValid: boolean;
    errMessage: string | null;
}

export default class ValidationField extends React.Component<IValidationFieldDescriptor, IValidationFieldState> {
    constructor(props: IValidationFieldDescriptor) {
        super(props);

        this.state = {
            errMessage: null,
            isValid: true,
        };

    }

    public componentDidMount() {
        this.validate(this.props.value).then((isValid) => this.props.updateFieldValidationStatus(isValid, this.props.fieldKey));
    }

    public render() {
        return (
            <div>
                {this.renderBaseElement()}
                {
                    !this.state.isValid
                        ? <div className="validation-error">{this.state.errMessage}</div>
                        : ""
                }
            </div>
        );
    }

    private renderBaseElement(): JSX.Element {
        if (this.props.type === ValidationFieldType.textarea) {
            return (
                <textarea
                    value={this.props.value}
                    onChange={(e) => this.props.changeHandler(e, this.props.fieldKey)
                        .then((value) => this.validate(value))
                        .then((isValid) => this.props.updateFieldValidationStatus(isValid, this.props.fieldKey))
                    }
                />
            );
        } else if (this.props.type === ValidationFieldType.text) {
            return (
                <input type="text"
                    value={this.props.value}
                    onChange={(e) => this.props.changeHandler(e, this.props.fieldKey)
                        .then((value) => this.validate(value))
                        .then((isValid) => this.props.updateFieldValidationStatus(isValid, this.props.fieldKey))
                    }
                />
            );
        } else {
            throw new TypeError("The type of validating field is unknown");
        }
    }

    private validate(value: string | null): Promise<boolean> {
        return new Promise((resolve, reject) => {
            if (this.props.isRequired && (value === null || value === "" || value === undefined)) {
                this.setState({
                    errMessage: "Field is required!",
                    isValid: false,
                });
                resolve(false);
            } else if (this.props.regExp && value && !this.props.regExp.test(value as string)) {
                this.setState({
                    errMessage: this.props.errMsg != null ? this.props.errMsg as string : "Invalid input",
                    isValid: false,
                });
                resolve(false);
            } else {
                this.setState({
                    errMessage: null,
                    isValid: true,
                });
                resolve(true);
            }
        });
    }
}
