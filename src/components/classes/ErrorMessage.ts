export default class ErrorMessage {
    public message: string = "";

    constructor(message: string) {
        this.message = message;
    }
}
