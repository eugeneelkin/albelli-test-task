import * as React from "react";
import * as ReactDOM from "react-dom";
import ErrorMessage from "./classes/ErrorMessage";
import DataColumn from "./dataColumn";
import {IComponentWithItemsDescriptor} from "./descriptors/propertyDescriptors";

export interface IDataRowDescriptor extends IComponentWithItemsDescriptor {
    rowClickCallback: (e: React.MouseEvent<HTMLTableRowElement>, rowColumns: Array<string|number>|ErrorMessage) => void;
    selectedId: string | number | null;
    idKey: string;
}

export default class DataRow extends React.Component<IDataRowDescriptor> {
    public render() {
        const idKey = "id";
        return (
            <tr
                className={this.props.selectedId && this.props.selectedId === this.props.items[this.props.idKey] ? "selected-row" : ""}
                onClick={(e) => this.props.rowClickCallback(e, this.props.items)}
            >
                {Object.keys(this.props.items).map((index) =>
                    <DataColumn content={this.props.items[index]} key={index} />,
                )}
            </tr>
        );
    }
}
