import * as React from "react";
import * as ReactDOM from "react-dom";
import * as PropertyDescriptors from "./descriptors/propertyDescriptors";

export default class AlertBox extends React.Component<PropertyDescriptors.IComponentDescriptor> {
    public render() {
        // TODO: Can be extended in future. Possible features: close event with crosshire, applying of different styles, etc.
        return (
            <div className="alert">{this.props.content}</div>
        );
    }
}
