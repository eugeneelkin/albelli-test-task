import * as React from "react";
import * as ReactDOM from "react-dom";
import DataRow, {IDataRowDescriptor} from "./dataRow";

export default class DataRows extends React.Component<IDataRowDescriptor> {
    public render() {
        return (
            <tbody>
                {Object.keys(this.props.items).map((index) =>
                    <DataRow
                        items={this.props.items[index]}
                        key={index}
                        rowClickCallback={this.props.rowClickCallback}
                        selectedId={this.props.selectedId}
                        idKey="id"
                    />,
                )}
            </tbody>
        );
    }
}
