import ErrorMessage from "../classes/errorMessage";

export interface IComponentDescriptor {
    content: string|number;
}

export interface IComponentWithItemsDescriptor {
    items: Array<string|number>|ErrorMessage;
}
