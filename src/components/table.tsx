import * as React from "react";
import * as ReactDOM from "react-dom";
import AlertBox from "./alertBox";
import ErrorMessage from "./classes/errorMessage";
import {IDataRowDescriptor} from "./dataRow";
import DataRows from "./dataRows";
import * as PropertyDescriptors from "./descriptors/propertyDescriptors";
import HeaderRow from "./headerRow";

export interface ITableDescriptor extends IDataRowDescriptor {
    headers: Array<string|number>;
}

export default class Table extends React.Component<ITableDescriptor> {
    public render() {
        if (this.props.items instanceof ErrorMessage) {
            return (
                <AlertBox content={(this.props.items as ErrorMessage).message} />
            );
        }

        return (
            <table className="items-table">
                <HeaderRow items={this.props.headers} />
                <DataRows
                    items={this.props.items}
                    rowClickCallback={this.props.rowClickCallback}
                    selectedId={this.props.selectedId}
                    idKey="id"
                />
                <tfoot />
            </table>
        );
    }
}
