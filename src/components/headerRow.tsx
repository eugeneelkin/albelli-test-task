import * as React from "react";
import * as ReactDOM from "react-dom";
import * as PropertyDescriptors from "./descriptors/propertyDescriptors";
import HeaderColumn from "./headerColumn";

export default class HeaderRow extends React.Component<PropertyDescriptors.IComponentWithItemsDescriptor> {
    public render() {
        return (
            <thead>
                <tr>
                    {Object.keys(this.props.items).map((index) =>
                        <HeaderColumn content={this.props.items[index]} key={index} />,
                    )}
                </tr>
            </thead>
        );
    }
}
