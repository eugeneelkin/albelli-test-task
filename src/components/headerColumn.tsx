import * as React from "react";
import * as ReactDOM from "react-dom";
import * as PropertyDescriptors from "./descriptors/propertyDescriptors";

export default class HeaderColumn extends React.Component<PropertyDescriptors.IComponentDescriptor> {
    public render() {
        return <th>{this.props.content}</th>;
    }
}
