import * as React from "react";
import * as ReactDOM from "react-dom";
import {IComponentDescriptor} from "./descriptors/propertyDescriptors";

export default class DataColumn extends React.Component<IComponentDescriptor> {
    public render() {
        if (typeof this.props.content === "number") {
            return <td className="table-cell-numeric">{this.props.content}</td>;
        }

        return <td>{this.props.content}</td>;
    }
}
