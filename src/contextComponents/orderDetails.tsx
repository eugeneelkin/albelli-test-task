import * as React from "react";
import * as ReactDOM from "react-dom";
import AlertBox from "../components/alertBox";
import Button from "../components/button";
import ErrorMessage from "../components/classes/errorMessage";
import ValidationField, { ValidationFieldType } from "../components/validationField";
import { ComponentViewMode } from "../enums/componentViewMode";
import AppSettingsManager from "../helpers/appSettingsManager";
import LocalizationManager from "../helpers/localizationManager";

interface IOrderDetailsDescriptor {
    order: IDetailedOrder | ErrorMessage | null;
    viewMode: ComponentViewMode;
    onClickEditAddressCallback: (e: React.MouseEvent<HTMLSpanElement>) => void;
    onClickCancelEditAddressCallback: (e: React.MouseEvent<HTMLSpanElement>) => void;
    onChangeAddressFieldCallback: (e: any, fieldKey: string) => Promise<string>;
    onClickSaveAddressCallback: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

export interface IDetailedOrder {
    id: string | number;
    customerName: string;
    priceInEuros: number;
    tickets: IDetailedOrderTicket[];
    address: IDetaildeOrderAddress;
}

export interface IDetailedOrderTicket {
    id: string | number;
    name: string;
}

export interface IDetaildeOrderAddress {
    address1: string;
    address2: string;
    city: string;
    state: string;
    country: string;
    zipcode: string;
    houseNum: string;
    phoneNum: string;
}

interface IDetaildeOrderState {
    invalidFields: string[];
}

export default class OrderDetails extends React.Component<IOrderDetailsDescriptor, IDetaildeOrderState> {
    private localizationManager: LocalizationManager;
    private appSettingsManager: AppSettingsManager;
    private hasErrors: boolean = false;

    constructor(props: IOrderDetailsDescriptor) {
        super(props);

        this.state = {
            invalidFields: [],
        };

        this.localizationManager = new LocalizationManager();
        this.appSettingsManager = new AppSettingsManager();
    }

    public render() {
        if (this.props.order == null) {
            return ("");
        }

        if (this.props.order instanceof ErrorMessage) {
            return (
                <AlertBox content={(this.props.order as ErrorMessage).message} />
            );
        }

        const tickets: IDetailedOrderTicket[] = this.props.order.tickets;

        return (
            <div>
                <h2>{this.tr("UI_ORDER_DETAILS_TITLE").replace("{0}", this.props.order.id.toString())}</h2>
                <div className="form-grid-container">
                    <div><b>{this.tr("UI_ORDER_DETAILS_CUSTOMER_NAME")}</b></div><div>{this.props.order.customerName}</div>
                    <div><b>{this.tr("UI_ORDER_DETAILS_PRICE")}</b></div><div>{this.props.order.priceInEuros}</div>
                </div>
                <form>
                    <fieldset className="fieldset-address">
                        <legend>{this.tr("UI_ORDER_DETAILS_ADDRESS")}</legend>
                        {(this.props.viewMode === ComponentViewMode.Edit)
                            ? <div className="fieldset-edit-box">
                                <span onClick={(e) => this.props.onClickCancelEditAddressCallback(e)}>{this.tr("UI_ORDER_DETAILS_CANCEL")}</span>
                            </div>
                            : <div className="fieldset-edit-box">
                                <span onClick={(e) => this.props.onClickEditAddressCallback(e)}>{this.tr("UI_ORDER_DETAILS_EDIT_ADDRESS")}</span>
                            </div>
                        }
                        <div className="form-grid-container">
                            <div><b>{this.tr("UI_ORDER_DETAILS_ADDRESS1")}</b>
                                {this.props.viewMode === ComponentViewMode.Edit ? " *" : ""}
                            </div>
                            <div>
                                {(this.props.viewMode === ComponentViewMode.Edit)
                                    ?
                                    <ValidationField
                                        type={ValidationFieldType.textarea}
                                        value={this.props.order.address.address1}
                                        isRequired={true}
                                        changeHandler={this.props.onChangeAddressFieldCallback}
                                        fieldKey="address1"
                                        updateFieldValidationStatus={this.updateFieldValidationStatus.bind(this)}
                                    />
                                    : this.props.order.address.address1
                                }
                            </div>
                            <div><b>{this.tr("UI_ORDER_DETAILS_ADDRESS2")}</b></div>
                            <div>
                                {(this.props.viewMode === ComponentViewMode.Edit)
                                    ? <ValidationField
                                        type={ValidationFieldType.textarea}
                                        value={this.props.order.address.address2}
                                        changeHandler={this.props.onChangeAddressFieldCallback}
                                        fieldKey="address2"
                                        updateFieldValidationStatus={this.updateFieldValidationStatus.bind(this)}
                                    />
                                    : this.props.order.address.address2
                                }
                            </div>
                            <div><b>{this.tr("UI_ORDER_DETAILS_HOUSE_NUM")}</b>
                                {this.props.viewMode === ComponentViewMode.Edit ? " *" : ""}
                            </div>
                            <div>
                                {(this.props.viewMode === ComponentViewMode.Edit)
                                    ? <ValidationField
                                        type={ValidationFieldType.text}
                                        value={this.getInitedValue(this.props.order.address.houseNum)}
                                        isRequired={true}
                                        changeHandler={this.props.onChangeAddressFieldCallback}
                                        fieldKey="houseNum"
                                        updateFieldValidationStatus={this.updateFieldValidationStatus.bind(this)}
                                        errMsg={this.tr("UI_ORDER_DETAILS_VALIDATION_HOUSE_NUMBER")}
                                        regExp={/^\d{1,100}$/i}
                                    />
                                    : this.props.order.address.houseNum
                                }
                            </div>
                            <div><b>{this.tr("UI_ORDER_DETAILS_CITY")}</b></div>
                            <div>
                                {(this.props.viewMode === ComponentViewMode.Edit)
                                    ? <ValidationField
                                        type={ValidationFieldType.text}
                                        value={this.props.order.address.city}
                                        changeHandler={this.props.onChangeAddressFieldCallback}
                                        fieldKey="city"
                                        updateFieldValidationStatus={this.updateFieldValidationStatus.bind(this)}
                                    />
                                    : this.props.order.address.city
                                }
                            </div>
                            <div><b>{this.tr("UI_ORDER_DETAILS_STATE")}</b></div>
                            <div>
                                {(this.props.viewMode === ComponentViewMode.Edit)
                                    ? <ValidationField
                                        type={ValidationFieldType.text}
                                        value={this.props.order.address.state}
                                        changeHandler={this.props.onChangeAddressFieldCallback}
                                        fieldKey="state"
                                        updateFieldValidationStatus={this.updateFieldValidationStatus.bind(this)}
                                    />
                                    : this.props.order.address.state
                                }
                            </div>
                            <div><b>{this.tr("UI_ORDER_DETAILS_COUNTRY")}</b>
                                {this.props.viewMode === ComponentViewMode.Edit ? " *" : ""}
                            </div>
                            <div>
                                {(this.props.viewMode === ComponentViewMode.Edit)
                                    ? <ValidationField
                                        type={ValidationFieldType.text}
                                        value={this.props.order.address.country}
                                        isRequired={true}
                                        changeHandler={this.props.onChangeAddressFieldCallback}
                                        fieldKey="country"
                                        updateFieldValidationStatus={this.updateFieldValidationStatus.bind(this)}
                                    />
                                    : this.props.order.address.country
                                }
                            </div>
                            <div><b>{this.tr("UI_ORDER_DETAILS_ZIP_CODE")}</b>
                                {this.props.viewMode === ComponentViewMode.Edit ? " *" : ""}
                            </div>
                            <div>
                                {(this.props.viewMode === ComponentViewMode.Edit)
                                    ? <ValidationField
                                        type={ValidationFieldType.text}
                                        value={this.props.order.address.zipcode}
                                        isRequired={true}
                                        changeHandler={this.props.onChangeAddressFieldCallback}
                                        fieldKey="zipcode"
                                        updateFieldValidationStatus={this.updateFieldValidationStatus.bind(this)}
                                        errMsg={this.tr("UI_ORDER_DETAILS_VALIDATION_ZIP_CODE")}
                                        regExp={/^\d{4} [A-Z]{2}$/i}
                                    />
                                    : this.props.order.address.zipcode
                                }
                            </div>
                            <div><b>{this.tr("UI_ORDER_DETAILS_PHONE_NUM")}</b>
                                {this.props.viewMode === ComponentViewMode.Edit ? " *" : ""}
                            </div>
                            <div>
                                {(this.props.viewMode === ComponentViewMode.Edit)
                                    ? <ValidationField
                                        type={ValidationFieldType.text}
                                        value={this.getInitedValue(this.props.order.address.phoneNum)}
                                        isRequired={true}
                                        changeHandler={this.props.onChangeAddressFieldCallback}
                                        fieldKey="phoneNum"
                                        updateFieldValidationStatus={this.updateFieldValidationStatus.bind(this)}
                                    />
                                    : this.props.order.address.phoneNum
                                }
                            </div>
                        </div>
                        {(this.props.viewMode === ComponentViewMode.Edit)
                            ? <Button
                                className="button-add"
                                content={this.tr("UI_ORDER_DETAILS_SAVE")}
                                onClickCallback={this.props.onClickSaveAddressCallback}
                                disabled={this.state.invalidFields.length > 0}
                            />
                            : ""
                        }
                        <div className="clear-both"></div>
                    </fieldset>
                </form>
                <fieldset>
                    <legend>{this.tr("UI_ORDER_DETAILS_TICKETS")}</legend>
                    {(tickets.length === 0)
                        ? <div>{this.tr("UI_ORDER_DETAILS_NO_TICKETS")}</div>
                        : Object.keys(tickets).map((index) =>
                            <div key={index}>
                                <a href={this.appSettingsManager.ticketPath.concat(tickets[index].id)}>{tickets[index].id}</a>:
                                {tickets[index].name}
                            </div>,
                        )
                    }
                </fieldset>
            </div>);
    }

    private getInitedValue(value: any): string {
        return value ? value as string : "";
    }

    private updateFieldValidationStatus(isValid: boolean, fieldKey: string) {
        let newInvalids: string[] = this.state.invalidFields.slice();
        const targetIndex: number = newInvalids.indexOf(fieldKey);

        if (!isValid && targetIndex === -1) {
            newInvalids = [...this.state.invalidFields, fieldKey];
        } else if (isValid && targetIndex > -1) {
            newInvalids.splice(targetIndex, 1);
        }

        this.setState({ invalidFields: newInvalids });
    }

    // Short representation of localized string receiving. tr -- translate
    private tr(key: string) {
        return this.localizationManager.getString(key);
    }
}
