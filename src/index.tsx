import axios from "axios";
import * as React from "react";
import * as ReactDOM from "react-dom";
import AlertBox from "./components/alertBox";
import ErrorMessage from "./components/classes/errorMessage";
import Table from "./components/table";
import OrderDetails, { IDetaildeOrderAddress, IDetailedOrder } from "./contextComponents/orderDetails";
import { ComponentViewMode } from "./enums/componentViewMode";
import DataConverter from "./helpers/dataConverter";
import LocalizationManager from "./helpers/localizationManager";
import "./index.css";

interface IAppStateData {
    headers: string[];
    items: Array<string | number> | ErrorMessage;
}

interface IAppState {
    data: IAppStateData;
    targetOrder: IDetailedOrder | ErrorMessage | null;
    backupOrder: IDetailedOrder | null;
    targetOrderMode: ComponentViewMode;
}

interface IPatchCommand {
    op: string;
    path: string;
    value: string;
}

class AppFrame extends React.Component<{}, IAppState> {
    private localizationManager: LocalizationManager;

    constructor(props: {}) {
        super(props);
        this.state = {
            backupOrder: null,
            data: {
                headers: [],
                items: [],
            },
            targetOrder: null,
            targetOrderMode: ComponentViewMode.View,
        };

        this.localizationManager = new LocalizationManager();
    }

    public componentDidMount() {
        axios.get(`/api/orders`)
            .then(
                (response) => {
                    const convertedOrders = response.data.map((x: any) => DataConverter.convert(x, this.getWhiteListFields()));

                    this.setState(
                        {
                            data: {
                                headers: this.getHeaders(),
                                items: convertedOrders,
                            },
                        });
                },
                (error) => {
                    this.setState(
                        {
                            data: {
                                headers: this.getHeaders(),
                                items: new ErrorMessage(this.tr("ERR_AJAX_ORDER_LIST")),
                            },
                            targetOrder: null,
                            targetOrderMode: ComponentViewMode.View,
                        });

                });
    }

    public render() {

        const selectedId: string | number | null = this.state.targetOrder && !(this.state.targetOrder instanceof ErrorMessage)
            ? this.state.targetOrder.id
            : null;

        return (
            <section className="grid-container">
                <section className="grid-item grid-item-caption"><h1>Orders Management</h1></section>
                <section className="grid-item grid-item-filters">
                    There will be a panel with some help info about data filtration (when filtration will be implemented)
                </section>
                <section className="grid-item grid-item-data">
                    <Table
                        headers={this.state.data.headers}
                        items={this.state.data.items}
                        rowClickCallback={this.clickRow.bind(this)}
                        selectedId={selectedId}
                        idKey="id"
                    />
                </section>
                <section className="grid-item grid-item-details">
                    <OrderDetails
                        order={this.state.targetOrder}
                        viewMode={this.state.targetOrderMode}
                        onClickEditAddressCallback={this.clickEditDetails.bind(this)}
                        onClickCancelEditAddressCallback={this.clickCancelEditDetails.bind(this)}
                        onChangeAddressFieldCallback={this.changeAddressField.bind(this)}
                        onClickSaveAddressCallback={this.clickSaveOrderAddress.bind(this)}
                    />
                </section>
            </section>
        );
    }

    // Short representation of localized string receiving. tr -- translate
    private tr(key: string) {
        return this.localizationManager.getString(key);
    }

    private getHeaders(): string[] {
        return ["Id", "Price (euro)", "Customer Name"];
    }

    private getWhiteListFields(): string[] {
        return ["id", "priceInEuros", "customerName"];
    }

    private clickRow(e: any, items: Array<string | number>) {
        e.preventDefault();
        const idKey: string = "id";

        axios.get(`/api/orders/` + items[idKey])
            .then(
                (response) => {
                    this.setState(
                        {
                            backupOrder: JSON.parse(JSON.stringify(response.data)),
                            targetOrder: response.data,
                            targetOrderMode: ComponentViewMode.View,
                        });
                },
                (error) => {
                    this.setState(
                        {
                            backupOrder: null,
                            targetOrder: new ErrorMessage(this.tr("ERR_AJAX_DETAILED_ORDER")),
                            targetOrderMode: ComponentViewMode.View,
                        });

                });
    }

    private clickEditDetails(e: any) {
        e.preventDefault();

        this.setState(
            {
                targetOrderMode: ComponentViewMode.Edit,
            });
    }

    private clickCancelEditDetails(e: any) {
        e.preventDefault();

        if (this.state.backupOrder) {
            const backupOrder: IDetailedOrder = JSON.parse(JSON.stringify(this.state.backupOrder));

            this.setState(
                {
                    targetOrder: backupOrder,
                    targetOrderMode: ComponentViewMode.View,
                });
        }
    }

    private changeAddressField(e: any, fieldKey: string): Promise<string> {
        e.preventDefault();

        return new Promise((resolve, reject) => {
            const targetOrder: IDetailedOrder = JSON.parse(JSON.stringify(this.state.targetOrder));
            targetOrder.address[fieldKey] = e.target.value;

            this.setState({targetOrder});

            resolve(e.target.value);
        });
    }

    private clickSaveOrderAddress(e: any) {
        e.preventDefault();

        const targetOrder: IDetailedOrder = this.state.targetOrder as IDetailedOrder;
        const targetAddress: IDetaildeOrderAddress = targetOrder.address;

        const patchCommands: IPatchCommand[] = Object.keys(targetAddress).map((index) => {
            return {
                op: "replace",
                path: "/address/" + index,
                value: targetAddress[index],
            };
        });

        axios.patch(`/api/orders/` + targetOrder.id, patchCommands).then(
            (response) => {
                this.setState(
                    {
                        targetOrderMode: ComponentViewMode.View,
                    });
            },
        );
    }
}

ReactDOM.render(<AppFrame />, document.getElementById("root"));
