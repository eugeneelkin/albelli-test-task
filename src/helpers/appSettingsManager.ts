export default class AppSettingsManager {
    private settings: object;

    constructor() {
        this.settings = require("../appSettings.json");
    }

    public get ticketPath(): string {
        const ticketPathKey = "ticketPath";
        return this.settings[ticketPathKey];
    }
}
