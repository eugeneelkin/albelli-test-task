export default class LocalizationManager {
    private defaultStrings: object;

    constructor() {
        this.defaultStrings = require("../dictionaries/default.json");
    }

    public getString(key: string): string {
        // TODO: figure out how implement exactly local string, ru-RU, en-EN, etc
        return this.defaultStrings[key];
    }
}
