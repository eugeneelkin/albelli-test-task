export default class DataConverter {
    public static convert(rawData: any, whiteListProperties: string[]): object {
        const filteredData: object = {};

        for (const property in rawData) {
            if (rawData.hasOwnProperty(property) && whiteListProperties.indexOf(property) > -1) {
                Object.defineProperty(filteredData, property, {
                    configurable: true,
                    enumerable: true,
                    value: rawData[property],
                    writable: true,
                });
            }
        }

        return filteredData;
    }
}
