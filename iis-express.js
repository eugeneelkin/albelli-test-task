var execFile = require('child_process').execFile;

var iisExpressExe = 'C:\\Program Files\\IIS Express\\iisexpress.exe';

var args = [
    '/port:5000',
    '/path:' + __dirname + '\\dist_api'
];

var childProcess = execFile(iisExpressExe, args, {});

childProcess.stdout.on('data', function(data) {
    console.log(removeTrailingLinebreak(data));
});

childProcess.stderr.on('data', function(data) {
    console.log(removeTrailingLinebreak(data));
});

var removeTrailingLinebreak = function (input) {
    return input.replace(/\s+$/, '');
}