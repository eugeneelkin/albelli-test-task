﻿namespace DataInstructions.Instructions
{
    using System.Threading.Tasks;

    public interface IOperationInstruction<TResult>
    {
        Task<TResult> Execute();
    }
}
