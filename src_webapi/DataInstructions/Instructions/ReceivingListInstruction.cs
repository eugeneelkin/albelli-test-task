﻿namespace DataInstructions.Instructions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using BaseEntities.Entities;
    using DataInstructions.Extensions;
    using Microsoft.EntityFrameworkCore;

    public class ReceivingListInstruction<TEntity, TId> : IOperationInstruction<IEnumerable<TEntity>>
        where TEntity : class, IEntityWithId<TId>, new()
    {
        private readonly DbContext context;
        private readonly Expression<Func<TEntity, bool>> filterFunction = null;
        private readonly string orderByField = null;
        private readonly bool isDescending = false;
        private readonly int? pageSize = null;
        private readonly int? pageAt = null;
        private readonly string[] navigationProperties = null;

        public ReceivingListInstruction(DbContext context)
        {
            this.context = context;
        }

        public ReceivingListInstruction(DbContext context, Expression<Func<TEntity, bool>> filterFunction): this(context)
        {
            this.filterFunction = filterFunction;
        }

        public ReceivingListInstruction(DbContext context, string orderByField, bool isDescending, int? pageAt, int? pageSize, Expression<Func<TEntity, bool>> filterFunction, string[] navigationProperties): this(context, filterFunction)
        {
            this.orderByField = orderByField;
            this.isDescending = isDescending;
            this.pageAt = pageAt;
            this.pageSize = pageSize;
            this.navigationProperties = navigationProperties;
        }

        public async Task<IEnumerable<TEntity>> Execute()
        {
            var dbSet = this.context.Set<TEntity>();
            IQueryable<TEntity> items = dbSet;

            if (this.navigationProperties != null && this.navigationProperties.Length > 0)
            {
                foreach (var navProp in this.navigationProperties)
                {
                    items = items.Include(navProp);
                }
            }

            if (!string.IsNullOrEmpty(this.orderByField))
            {
                items = this.isDescending ? items.OrderByDescending(this.orderByField) : items.OrderBy(this.orderByField);
            }

            if (this.filterFunction != null)
            {
                items = items.Where(this.filterFunction);
            }

            if (this.pageAt.HasValue && this.pageSize.HasValue)
            {
                items = items.Skip(this.pageSize.Value * (this.pageAt.Value - 1)).Take(this.pageSize.Value);
            }

            return await Task.FromResult(items.ToList());
        }
    }
}
