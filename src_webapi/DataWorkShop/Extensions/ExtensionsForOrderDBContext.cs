﻿namespace DataWorkShop.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using DataWorkShop.Entities;
    using Microsoft.EntityFrameworkCore;
    using System;

    public static class ExtensionsForOrderDBContext
    {
        public static void EnsureSeedData(this OrderDBContext context)
        {
            // Apply seeding only if tables are empty.
            // Also, we check that there is any migration because seeding can be triggered during "Add-Migration InitialCreate" command.
            if (context.Database.GetAppliedMigrations().Any()
                && !context.Database.GetPendingMigrations().Any()
                && !context.Orders.Any())
            {
                // Orders seeding
                Random rnd = new Random();

                var orderIds = new string[]
                {
                    "100756",
                    "1003221",
                    "1005239",
                };
                var ticketsIds = new string[]
                {
                    "CC-1",
                    "CC-2",
                    "CC-3",
                    "CC-4"
                };
                var ticketsNames = new string[] {
                    "Call customer to reupload blurred images",
                    "Customer refund - bank account",
                    "Send order to another address",
                    "Notify about cancelling the order"
                };

                for (var i = 0; i < 10; i++)
                {
                    var order = new Order
                    {
                        Id = "SAL" + (i < 3 ? orderIds[i] : rnd.Next(10000000, 90000000).ToString()),
                        CustomerName = (i < 3 ? "Customer with ticket " : "Customer ") + i.ToString(),
                        PriceInEuros = rnd.Next(100, 900),
                        Address = new OrderAddress
                        {
                            Address1 = "Some adress line 1 " + rnd.Next(10000, 90000),
                            Address2 = "Some adress line 2 " + rnd.Next(10000, 90000),
                            City = "Some city " + rnd.Next(10000, 90000),
                            Country = "Some country " + rnd.Next(10000, 90000),
                            State = "Some state " + rnd.Next(10000, 90000),
                            Zipcode = "Some zip code " + rnd.Next(10000, 90000)
                        }
                    };

                    if (i < 3)
                    {
                        order.Tickets = new List<OrderTicket>()
                            {
                                new OrderTicket
                                {
                                    Id = ticketsIds[i],
                                    Name = ticketsNames[i]
                                }
                            };

                        if (i == 2)
                        {
                            order.Tickets.Add(
                                new OrderTicket
                                {
                                    Id = ticketsIds[i + 1],
                                    Name = ticketsNames[i + 1]
                                });
                        }
                    }

                    context.Orders.Add(order);
                    context.SaveChanges();
                }
            }
        }
    }
}
