﻿namespace DataWorkShop
{
    using DataWorkShop.Entities;
    using Microsoft.EntityFrameworkCore;

    public class OrderDBContext: DbContext
    {
        public OrderDBContext() : base()
        {

        }

        public OrderDBContext(DbContextOptions options): base(options)
        {

        }

        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderTicket> OrderTickets { get; set; }
        public virtual DbSet<OrderAddress> OrderAdresses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>()
                .HasOne(a => a.Address)
                .WithOne(b => b.Order)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
