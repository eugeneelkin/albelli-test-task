﻿namespace DataWorkShop.Entities
{
    using System.ComponentModel.DataAnnotations.Schema;
    using BaseEntities.Entities;

    public class OrderTicket: BaseEntity<string>
    {
        public OrderTicket()
        {
        }

        public string Name { get; set; }

        public string OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
    }
}
