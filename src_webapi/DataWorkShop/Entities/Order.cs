﻿namespace DataWorkShop.Entities
{
    using System.Collections.Generic;
    using BaseEntities.Entities;

    public class Order: BaseEntity<string>
    {
        public Order()
        {
            this.Tickets = new List<OrderTicket>();
        }

        public double PriceInEuros { get; set; }
        public string CustomerName { get; set; }

        public virtual ICollection<OrderTicket> Tickets { get; set; }

        public virtual OrderAddress Address { get; set; }
    }
}
