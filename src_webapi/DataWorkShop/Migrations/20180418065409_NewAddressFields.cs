﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DataWorkShop.Migrations
{
    public partial class NewAddressFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HouseNum",
                table: "OrderAdresses",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNum",
                table: "OrderAdresses",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HouseNum",
                table: "OrderAdresses");

            migrationBuilder.DropColumn(
                name: "PhoneNum",
                table: "OrderAdresses");
        }
    }
}
