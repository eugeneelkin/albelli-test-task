﻿namespace DataInstructions.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BaseEntities.Entities;
    using DataInstructions.Instructions;
    using DataInstructions.Tests.Exceptions;
    using DataWorkShop;
    using DataWorkShop.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    [TestClass]
    public class InstructionsTests
    {
        Mock<OrderDBContext> dbContextMock = null;
        IDictionary<string, byte[]> rowVersions = new Dictionary<string, byte[]>();
        IList<Order> ordersList = new List<Order>();

        [TestInitialize]
        public void Start()
        {
            Random rnd = new Random();
            for (var i = 0; i < 10; i++)
            {
                var rowVersion = UTF8Encoding.ASCII.GetBytes((i + 1).ToString());
                var order = new Order
                {
                    Id = (i + 1).ToString(),
                    CustomerName = "Customer " + i.ToString(),
                    PriceInEuros = rnd.Next(100, 900),
                    Address = new OrderAddress
                    {
                        Address1 = "Some adress line 1 " + rnd.Next(10000, 90000),
                        Address2 = "Some adress line 2 " + rnd.Next(10000, 90000),
                        City = "Some city " + rnd.Next(10000, 90000),
                        Country = "Some country " + rnd.Next(10000, 90000),
                        State = "Some state " + rnd.Next(10000, 90000),
                        Zipcode = "Some zip code " + rnd.Next(10000, 90000)
                    },
                    RowVersion = rowVersion
                };

                rowVersions.Add((i + 1).ToString(), rowVersion);
                this.ordersList.Add(order);
            }

            var ordersDbSet = GetQueryableMockDbSet<Order, string>(this.ordersList);
            this.dbContextMock = new Mock<OrderDBContext>();
            this.dbContextMock.Setup(x => x.Orders).Returns(ordersDbSet);
            this.dbContextMock.Setup(x => x.Set<Order>()).Returns(ordersDbSet);
            this.dbContextMock.Setup(x => x.Remove(It.IsAny<Order>())).Callback<Order>((s) => {
                var initialRowVersion = this.rowVersions[s.Id];

                if (!initialRowVersion.SequenceEqual(s.RowVersion))
                {
                    throw new DbUpdateConcurrencyTestException();
                }

                ordersDbSet.Remove(s);
            });
        }

        [TestMethod]
        public async Task RemoveEntityByIdSuccessfullTest()
        {
            var context = this.dbContextMock.Object;
            Assert.AreEqual(context.Orders.Count(), 10);
            await new RemovalInstruction<Order, string>(context, "3", "Mw==").Execute();
            Assert.AreEqual(context.Orders.Count(), 9);
        }

        [TestMethod]
        [ExpectedException(typeof(DbUpdateConcurrencyTestException))]
        public async Task RemoveEntityByIdWronRowVersionTest()
        {
            var context = this.dbContextMock.Object;
            Assert.AreEqual(context.Orders.Count(), 10);
            await new RemovalInstruction<Order, string>(context, "3", "Dw==").Execute();
        }

        private static DbSet<TEntity> GetQueryableMockDbSet<TEntity, TId>(IList<TEntity> sourceList)
            where TEntity: class, IEntityWithId<TId>, new()
        {
            var queryable = sourceList.AsQueryable();

            var dbSet = new Mock<DbSet<TEntity>>();
            dbSet.As<IQueryable<TEntity>>().Setup(m => m.Provider).Returns(queryable.Provider);
            dbSet.As<IQueryable<TEntity>>().Setup(m => m.Expression).Returns(queryable.Expression);
            dbSet.As<IQueryable<TEntity>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            dbSet.As<IQueryable<TEntity>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());
            dbSet.Setup(d => d.Add(It.IsAny<TEntity>())).Callback<TEntity>((s) => sourceList.Add(s));
            dbSet.Setup(d => d.Remove(It.IsAny<TEntity>())).Callback<TEntity>((s) => sourceList.Remove(s));
            dbSet.Setup(d => d.Attach(It.IsAny<TEntity>())).Callback<TEntity>((s) =>
            {
                var index = sourceList.IndexOf(sourceList.FirstOrDefault(x => x.Id.Equals(s.Id)));
                sourceList[index] = s;
            });

            return dbSet.Object;
        }
    }
}
