﻿namespace BaseEntities.Entities
{
    public interface IEntityWithId<T>: IEmptyEntity
    {
        T Id { get; set; }
    }
}
