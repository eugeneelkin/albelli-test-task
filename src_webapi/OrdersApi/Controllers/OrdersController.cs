﻿namespace OrdersApi.Controllers
{
    using System.Threading.Tasks;
    using AutoMapper;
    using DataInstructions.Instructions;
    using DataWorkShop;
    using DataWorkShop.Entities;
    using Microsoft.AspNetCore.JsonPatch;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using OrdersApi.Models;

    [Produces("application/json")]
    [Route("api/Orders")]
    public class OrdersController : Controller
    {
        private readonly OrderDBContext context;

        public OrdersController(OrderDBContext context)
        {
            this.context = context;
        }

        // GET: api/Orders
        [HttpGet]
        public async Task<IActionResult> GetOrders(string orderByField = null, bool isDescending = false, int? pageSize = null, int? pageAt = null)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var orders = await new ReceivingListInstruction<Order, string>(this.context, orderByField, isDescending, pageAt, pageSize, null, new string[] {"Address"}).Execute();
            var sanitizedOrders = Mapper.Map<IEnumerable<Order>, IEnumerable<OrderViewModel>>(orders);

            return Ok(sanitizedOrders);
        }

        // GET: api/Orders/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrder([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var order = await new ReceivingInstruction<Order, string>(this.context, id, new string[] { "Address", "Tickets" }).Execute();

            if (order == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Order, OrderViewModel>(order));
        }

        // PUT: api/Orders/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrder([FromRoute] string id, [FromBody] Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != order.Id)
            {
                return BadRequest();
            }

            await new UpdateInstruction<Order, string>(this.context, id, order).Execute();

            return NoContent();
        }

        // PATCH: api/Orders/5
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchOrder([FromRoute] string id, [FromBody] JsonPatchDocument<Order> order)
        {
            await new PatchInstruction<Order, string>(this.context, id, order, new string[] { "Address" }).Execute();

            return NoContent();
        }

        // POST: api/Orders
        [HttpPost]
        public async Task<IActionResult> PostOrder([FromBody] Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var createdOrder = await new CreationInstruction<Order, string>(this.context, order).Execute();

            return CreatedAtAction("GetOrder", new { id = createdOrder.Id }, Mapper.Map<Order, OrderViewModel>(createdOrder));
        }

        // DELETE: api/Orders/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder([FromRoute] string id, string rowVersion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var orderAddress = new OrderAddress { Id = id };
            var order = new Order { Id = id, Address = orderAddress };

            await new RemovalInstruction<Order, string>(this.context, id, rowVersion).Execute();

            return Ok();
        }
    }
}