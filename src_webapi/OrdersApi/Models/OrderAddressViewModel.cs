﻿namespace OrdersApi.Models
{
    public class OrderAddressViewModel
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string HouseNum { get; set; }
        public string PhoneNum { get; set; }
    }
}
