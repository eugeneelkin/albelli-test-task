﻿namespace OrdersApi.Models
{
    public class OrderTicketViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
