﻿namespace OrdersApi.Models
{
    using System.Collections.Generic;

    public class OrderViewModel
    {
        public string Id { get; set; }
        public double PriceInEuros { get; set; }
        public string CustomerName { get; set; }
        public ICollection<OrderTicketViewModel> Tickets { get; set; }
        public OrderAddressViewModel Address { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
